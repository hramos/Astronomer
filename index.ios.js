/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TabBarIOS,
} from 'react-native';

// NavigatorIOS
import NativeTabBar from './NavigatorIOS/NativeTabBar';

// react-native-navigation
// import { Navigation } from 'react-native-navigation';
// import { registerScreens } from './screens';
// registerScreens();

const NavigationType = {
        NAVIGATION_EXPERIMENTAL: Symbol('NAVIGATION_EXPERIMENTAL'),
        NAVIGATORIOS: Symbol('NAVIGATORIOS'),
        REACT_NATIVE_NAVIGATION: Symbol('REACT_NATIVE_NAVIGATION'), // Note: You need to update AppDelegate.m and uncomment the block at the bottom of this file to enable/disable this
    }
const SELECTED_NAVIGATION_TYPE = NavigationType.NAVIGATORIOS;

import NavigationTabsExample from './NavigationExperimental/NavigationTabsExample.js'

class Astronomer extends Component {
  render() {
    switch (SELECTED_NAVIGATION_TYPE) {
      case NavigationType.NAVIGATION_EXPERIMENTAL:
        return (
          <NavigationTabsExample />
        )
      case NavigationType.NAVIGATORIOS:
        return (
          <NativeTabBar />
        )
      default:
      // react-native-navigation has a different entry point as RN is no longer the root element
      return null;
    }
  }
}

// react-native-navigation entry point -- has no effect unless AppDelegate.m is modified accordingly. uncomment to use
// Navigation.startTabBasedApp({
//   tabs: [
//     {
//       label: 'Solar System',
//       screen: 'astronomer.SolarSystemTabScreen',
//       icon: require('./img/solarSystem.png'),
//       title: 'Solar System'
//     },
//     {
//       label: 'Deep Space',
//       screen: 'astronomer.CatalogsTabScreen',
//       icon: require('./img/galaxy.png'),
//       title: 'Deep Space'
//     }
//   ],
//   tabsStyle: {
//     tabBarBackgroundColor: 'midnightblue',
//     tabBarSelectedButtonColor: 'white',
//     tabBarButtonColor: 'silver'
//   }
// });
//
// naive tabbar implementation entry point

AppRegistry.registerComponent('Astronomer', () => Astronomer);
