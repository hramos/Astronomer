'use strict';

var React = require('react');
var ReactNative = require('react-native');
var {
  StyleSheet,
  TabBarIOS,
  Text,
  View,
  NavigatorIOS,
} = ReactNative;

import ObservableTargetsList from '../screens/observableTargets';
import CatalogsList from '../screens/catalogs';

const MESSIER_DATA = require('../data/messier.json');
const SOLAR_SYSTEM_DATA = require('../data/solarSystem.json');


var DeepSpaceView = React.createClass({
  _handleBackButtonPress: function() {
    this.props.navigator.pop();
  },
  _handleNextButtonPress: function() {
    this.props.navigator.push(nextRoute);
  },
  render: function() {
    return (
      <ObservableTargetsList objects={this.props.objects} />
    )
  }
});

var NativeTabBar = React.createClass({
  statics: {
    title: '<TabBarIOS>',
    description: 'Tab-based navigation.',
  },

  displayName: 'TabBar',

  getInitialState: function() {
    return {
      selectedTab: 'solarSystemTab',
      favoritesCount: 0,
      presses: 0,
    };
  },

  _renderContent: function(objectType: string) {
    var objects = [];
    var title = `Astronomer`;
    switch (objectType) {
      case "dso":
        objects = MESSIER_DATA.objects;
        title = `Deep Space`
        break;
      case "solar":
        objects = SOLAR_SYSTEM_DATA.objects;
        title = `Solar System`
        break;
      default:
        objects = [];
    }
    return (
      <NavigatorIOS
      style={styles.container}
      titleTextColor="white"
      barTintColor="midnightblue"
        initialRoute={{
          title: title,
          component: ObservableTargetsList,
          passProps: { objects: objects }
        }} />
    );
  },

  _renderCatalog: function() {
    return (
      <NavigatorIOS
      style={styles.container}
      titleTextColor="white"
      barTintColor="midnightblue"
        initialRoute={{
          title: "Deep Space",
          component: CatalogsList
        }} />
    );
  },

  render: function() {
    return (
      <TabBarIOS
        unselectedTintColor="yellow"
        tintColor="white"
        barTintColor="midnightblue">
        <TabBarIOS.Item
          title="Solar System"
          icon={require('../img/solarSystem.png')}
          selected={this.state.selectedTab === 'solarSystemTab'}
          onPress={() => {
            this.setState({
              selectedTab: 'solarSystemTab'
            });
          }}>
          {this._renderContent('solar')}
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Deep Space"
          icon={require('../img/galaxy.png')}
          renderAsOriginal
          selected={this.state.selectedTab === 'dsoTab'}
          onPress={() => {
            this.setState({
              selectedTab: 'dsoTab',
            });
          }}>
          {this._renderCatalog()}
        </TabBarIOS.Item>
        {/* Favorites view not implemented, will do so once we have state */}
        <TabBarIOS.Item
          systemIcon="favorites"
          selected={this.state.selectedTab === 'favoritesTab'}
          badge={this.state.favoritesCount > 0 ? this.state.favoritesCount : undefined}
          onPress={() => {
            this.setState({
              selectedTab: 'favoritesTab'
            });
          }}>
          <Text>Not Implemented</Text>
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  },

});

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabContent: {
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'white',
    margin: 50,
  },
});

module.exports = NativeTabBar;
