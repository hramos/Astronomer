/**
 * @flow
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableOpacity,
} from 'react-native';

export default class ImageCellView extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.container}>
        <Image
          source={{uri: this.props.imageUri}}
          style={styles.thumbnail}
          resizeMode='contain'
        />
        <View style={styles.rightContainer}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.objectDescription}>{this.props.subTitle}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  rightContainer: {
    flex: 1,
  },
  objectDescription: {
    fontSize: 20,
    marginBottom: 8,
    textAlign: 'left',
    color: 'white'
  },
  title: {
    fontSize: 24,
    textAlign: 'left',
    color: 'dodgerblue'
  },
  thumbnail: {
    width: 180,
    height: 81,
  },
  listView: {
    paddingTop: 20,
    backgroundColor: 'black',
  },
});

AppRegistry.registerComponent('DeepSpaceObject', () => ImageCellView);
