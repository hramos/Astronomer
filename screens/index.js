// This file is only used by react-native-navigation

import { Navigation } from 'react-native-navigation';

import SolarSystemTabScreen from './solarSystemTargets.ios.js';
import CatalogsTabScreen from './catalogs.ios.js';
import DeepSpacePushedScreen from './deepSpaceTargets.ios.js';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('astronomer.SolarSystemTabScreen', () => SolarSystemTabScreen);
  Navigation.registerComponent('astronomer.CatalogsTabScreen', () => CatalogsTabScreen);
  Navigation.registerComponent('astronomer.DeepSpacePushedScreen', () => DeepSpacePushedScreen);
}
