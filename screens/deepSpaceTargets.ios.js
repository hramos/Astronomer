/**
 * Presents a list of observable targets
 * @flow
 */

import React, {
  AppRegistry,
  Component,
} from 'react-native';

import ObservableTargetsList from './observableTargets.ios.js';

const TARGETS_DATA = require('../data/messier.json');

export default class DeepSpaceTargetsList extends Component {
  render() {
    return (
      <ObservableTargetsList objects={TARGETS_DATA.objects}
      />
    )
  }
}

AppRegistry.registerComponent('DeepSpaceTargetsList', () => DeepSpaceTargetsList);
