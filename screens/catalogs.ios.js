/**
 * List of catalogs: Messier, Hershel, etc
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableOpacity,
} from 'react-native';

import ObservableTargetsList from './observableTargets';

const MESSIER_DATA = require('../data/messier.json');

const CATALOG_DATA = {
  "count": 1,
  "objects": [
    {
      "title": "Messier objects",
      "description": "The Messier objects are a set of over 100 astronomical objects first listed by French astronomer Charles Messier in 1771.",
      "imageUri": "http://messier.seds.org/Pics/M/110Ms.jpg",
      "total": MESSIER_DATA.total,
      "objects": MESSIER_DATA.objects
    },
    {
      "title": "New General Catalogue",
      "description": "The New Gegeral Catalogue of Nebulae and Clusters of Stars is a well-known catalogue of deep-sky objects compiled by John Louis Dreyer in 1888 as a new version of Hershel's catalogue.",
      "imageUri": "https://upload.wikimedia.org/wikipedia/commons/8/82/Catseyeandmore.jpg",
      "total": 0,
      "objects": []
    },
    {
      "title": "Hershel Catalogue",
      "description": "The Catalogue of Nebulae and Clusters of Stars is an astronomical catalogue of nebulae first published by William Herschel.",
      "imageUri": "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/NGC_2683_HST.jpg/1024px-NGC_2683_HST.jpg",
      "total": 0,
      "objects": []
    }

  ]
};

/*
this.props.navigator.push({
  screen: 'example.PushedScreen',
  title: 'Pushed Screen'
});
*/

export default class CatalogsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(CATALOG_DATA.objects),
      loaded: true,
    });
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderCatalog.bind(this)}
        style={styles.listView}
      />
    )
  }

  renderLoadingView() {
    return (
      <View style={styles.container}>
        <Text>
          Loading catalogs...
        </Text>
      </View>
    );
  }

  renderCatalog(catalog) {
    return (
      <TouchableOpacity style={styles.container} onPress={ this.onPress.bind(this, catalog.objects) }>
        <Image
          source={{uri: catalog.imageUri}}
          style={styles.thumbnail}
          resizeMode='stretch'
        />
        <View style={styles.rightContainer}>
          <Text style={styles.title}>{catalog.title}</Text>
          <Text style={styles.objectDescription}>{catalog.description}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  onPress(catalogObjects) {
    // NavigationExperimentalTabs way:
    // props.dispatch({ type: 'openDeepSpace', id: 'A' });

    // react-native-navigation way
    // this.props.navigator.push({
    //   screen: 'astronomer.DeepSpacePushedScreen',
    //   title: 'Messier',
    //   objects: CatalogsList
    // });

    // NavigatorIOS?
    this.props.navigator.push({
      title: "Messier",
      component: ObservableTargetsList,
      passProps: { objects: MESSIER_DATA.objects }
    });
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  rightContainer: {
    flex: 1,
  },
  objectDescription: {
    fontSize: 20,
    marginBottom: 8,
    textAlign: 'left',
    color: 'white'
  },
  title: {
    fontSize: 24,
    textAlign: 'left',
    color: 'dodgerblue'
  },
  thumbnail: {
    width: 60,
    height: 60,
  },
  listView: {
    paddingTop: 20,
    backgroundColor: 'black',
  },
});

AppRegistry.registerComponent('CatalogsList', () => CatalogsList);
