/**
 * Presents a list of observable targets
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  ListView,
} from 'react-native';

import ImageCellView from '../ImageCellView';

export default class ObservableTargetsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(this.props.objects),
      loaded: true,
    });
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderTarget}
        style={styles.listView}
      />
    )
  }

  renderLoadingView() {
    return (
      <View style={styles.container}>
        <Text>
          Loading targets...
        </Text>
      </View>
    );
  }

  renderTarget(target) {
    return (
      <ImageCellView
        title={target.title}
        subTitle={target.description}
        imageUri={target.imageUri}
      />
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  listView: {
    paddingTop: 20,
    backgroundColor: 'black',
  },
});

AppRegistry.registerComponent('ObservableTargetsList', () => ObservableTargetsList);
