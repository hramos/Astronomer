/**
 * Presents a list of observable targets
 * @flow
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
} from 'react-native';

import ObservableTargetsList from './observableTargets.ios.js';

const TARGETS_DATA = require('../data/solarSystem.json');

export default class SolarSystemTargetsList extends Component {
  render() {
    return (
      <ObservableTargetsList objects={TARGETS_DATA.objects}
      />
    )
  }
}

AppRegistry.registerComponent('SolarSystemTargetsList', () => SolarSystemTargetsList);
