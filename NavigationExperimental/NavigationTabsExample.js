/**
 * The examples provided by Facebook are for non-commercial testing and
 * evaluation purposes only.
 *
 * Facebook reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
'use strict';

const React = require('react');
const ReactNative = require('react-native');
const {
  NavigationExperimental,
  ScrollView,
  StyleSheet,
  View,
} = ReactNative;
const {
  Container: NavigationContainer,
  RootContainer: NavigationRootContainer,
  Reducer: NavigationReducer,
} = NavigationExperimental;

const NavigationExampleRow = require('./NavigationExampleRow');
const NavigationExampleTabBar = require('./NavigationExampleTabBar');

const SOLAR_SYSTEM_TAB_KEY = 'Solar System';
const DEEP_SPACE_TAB_KEY = 'Deep Space';

import CatalogsList from '../screens/catalogs.ios.js';
import SolarSystemTargetsList from '../screens/solarSystemTargets.ios.js';
import DeepSpaceTargetsList from '../screens/deepSpaceTargets.ios.js';

class ExampleTabPage extends React.Component {
  render() {
    const currentTabRoute = this.props.tabs[this.props.index];
    return (
      <ScrollView style={styles.tabPage}>
        <NavigationExampleRow
          text={`Current Tab is: ${currentTabRoute.key}`}
        />
        {this.props.tabs.map((tab, index) => (
          <NavigationExampleRow
            key={tab.key}
            text={`Go to ${tab.key}`}
            onPress={() => {
              this.props.onNavigate(NavigationReducer.TabsReducer.JumpToAction(index));
            }}
          />
        ))}
        <NavigationExampleRow
          text="Exit Tabs Example"
          onPress={this.props.onExampleExit}
        />
      </ScrollView>
    );
  }
}
ExampleTabPage = NavigationContainer.create(ExampleTabPage);

const ExampleTabsReducer = NavigationReducer.TabsReducer({
  tabReducers: [
    (lastRoute) => lastRoute || {key: SOLAR_SYSTEM_TAB_KEY, icon: require('../img/solarSystem.png') },
    (lastRoute) => lastRoute || {key: DEEP_SPACE_TAB_KEY, icon: require('../img/galaxy.png') },
  ],
});

class NavigationTabsExample extends React.Component {
  render() {
    return (
      <NavigationRootContainer
        reducer={ExampleTabsReducer}
        ref={navRootContainer => { this.navRootContainer = navRootContainer; }}
        renderNavigation={(navigationState) => {
          if (!navigationState) { return null; }

          switch (navigationState.children[navigationState.index].key) {
            case DEEP_SPACE_TAB_KEY:
              return (
                <View style={styles.topView}>
                  <DeepSpaceTargetsList />
                  <NavigationExampleTabBar
                    tabs={navigationState.children}
                    index={navigationState.index}
                  />
                </View>
              );
            default:
              return (
                <View style={styles.topView}>
                  <SolarSystemTargetsList />
                  <NavigationExampleTabBar
                    tabs={navigationState.children}
                    index={navigationState.index}
                  />
                </View>
              );
          }
        }}
      />
    );
  }
  handleBackAction() {
    return (
      this.navRootContainer &&
      this.navRootContainer.handleNavigation(NavigationRootContainer.getBackAction())
    );
  }
}

const styles = StyleSheet.create({
  topView: {
    flex: 1,
    paddingTop: 30,
  },
  tabPage: {
    backgroundColor: '#E9E9EF',
  },
});

module.exports = NavigationTabsExample;
